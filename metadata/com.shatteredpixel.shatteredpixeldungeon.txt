Categories:Games
License:GPLv3+
Web Site:http://shatteredpixel.tumblr.com/
Source Code:https://github.com/00-Evan/shattered-pixel-dungeon
Issue Tracker:https://github.com/00-Evan/shattered-pixel-dungeon/issues
Bitcoin:1LyLJAzxCfieivap1yK3iCpGoUmzAnjdyK

Auto Name:Shattered Pixel Dungeon
Summary:Rogue-like
Description:
Traditional roguelike game with pixel-art graphics and simple interface. Based
on [[com.watabou.pixeldungeon]].
.

Repo Type:git
Repo:https://github.com/00-Evan/shattered-pixel-dungeon

Build:0.2.2a,14
    commit=127c74696ade0be0c3824d9373f49b8d9b6e6179
    srclibs=PDClasses-shattered@f2bf9c65809f50ffd372f7d5e3df181cf970f892
    prebuild=cp -R $$PDClasses-shattered$$/com/watabou/* src/com/shatteredpixel/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-19

Build:0.2.2b,15
    commit=e58df1bc9ccb77fd3218775639650f75a5865172
    srclibs=PDClasses-shattered@f2bf9c65809f50ffd372f7d5e3df181cf970f892
    prebuild=cp -R $$PDClasses-shattered$$/com/watabou/* src/com/shatteredpixel/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-19

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.2.3-BETA3
Current Version Code:18

