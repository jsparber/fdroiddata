Categories:Navigation
License:GPLv3
Web Site:http://jkliemann.de/parkendd
Source Code:https://github.com/jklmnn/ParkenDD
Issue Tracker:https://github.com/jklmnn/ParkenDD/issues

Auto Name:ParkenDD
Summary:Show available parking slots in Dresden
Description:
Shows available parking lots in Dresden, Germany. Note that the app and the
homepage are only available in German at the moment.
.

Repo Type:git
Repo:https://github.com/jklmnn/ParkenDD

Build:0.1.0,1
    commit=71fdddb4509930af0b4fc202e9b42e18ac0f1525
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.2.0-alpha
Current Version Code:3

