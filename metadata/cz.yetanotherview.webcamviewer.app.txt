Categories:Internet,Multimedia
License:Apache2
Web Site:https://github.com/TomasValenta/WebCamViewer/blob/HEAD/README.md
Source Code:https://github.com/TomasValenta/WebCamViewer
Issue Tracker:https://github.com/TomasValenta/WebCamViewer/issues

Auto Name:WebCam Viewer
Summary:View webcam streams
Description:
Add and refresh webcams accessible from an URL.
.

Repo Type:git
Repo:https://github.com/TomasValenta/WebCamViewer

Build:2.0,25
    commit=fac88fdc9619330497f8554a548b22c63ad707a4
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.0
Current Version Code:25

